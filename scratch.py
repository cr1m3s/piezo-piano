#!/usr/bin/python3

from gpiozero import Button, TonalBuzzer, Buzzer

a = TonalBuzzer(21)

but1 = Button(26)
but2 = Button(16)
but3 = Button(19)
but4 = Button(13)
but5 = Button(6)
but6 = Button(12)

value = 0

while True:
    
    if but6.is_pressed:
        value += 1
        a.play(277.2)
    elif but5.is_pressed:
        value += 2
        a.play(311.1)
    elif but4.is_pressed:
        value +=3
        a.play(370.0)
    elif but3.is_pressed:
        value += 4
        a.play(415.3)
    elif but2.is_pressed:
        value +=5
        a.play(466.2)
    elif but1.is_pressed:
        value += 6
        a.play(493.9)
    else:
        print(value)
        a.stop()
        
    
